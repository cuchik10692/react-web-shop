import { required, checkLength } from '@utils/validates';

const validate = values => {
  let errors = {}
  const requiredFields = [
    'name',
    'price',
  ]
  const LengthFields = [
    {field: 'name', limit: 255, compare: 'max'},
    {field: 'price', limit: 1000000000, errorMsg: 'Giá phải dưới 1.000.000.000 VND', type: 'value', compare: 'max'},
  ]
  const requireError = required(requiredFields, values);
  const maxError = checkLength(LengthFields, values);
  errors = {...errors, ...requireError, ...maxError};
  return errors;
}

export default validate;