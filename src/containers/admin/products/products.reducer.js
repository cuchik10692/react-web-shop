import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form';

import { ProductsState } from './products.state'
import { ProductsTypes } from './products.types';
import ActivityStatus from "@common/enum/activity";

const initialProductsState = new ProductsState();

export const productsReducer = (state = initialProductsState, action) => {
  switch (action.type) {
    case ProductsTypes.FETCH_PRODUCTS.LOADING:
      return { ...state, products: [],
        activityStatus: ActivityStatus.Loading,
        error: null }
    case ProductsTypes.FETCH_PRODUCTS.SUCCESS:
      return { ...state, products: action.payload,
        activityStatus: ActivityStatus.Loaded,
        error: null }
    case ProductsTypes.FETCH_PRODUCTS.FAILURE:
      return { ...state, activityStatus: ActivityStatus.Loaded,
        error: null }

    case ProductsTypes.CREATE_PRODUCTS.LOADING:
      return { ...state, addProductStatus: ActivityStatus.Loading,
        createProductErorr: null }
    case ProductsTypes.CREATE_PRODUCTS.SUCCESS:
      return { ...state, addProductStatus: ActivityStatus.Loaded,
        createProductErorr: null }
    case ProductsTypes.CREATE_PRODUCTS.FAILURE:
      return { ...state, addProductStatus: ActivityStatus.Loaded,
        createProductErorr: action.payload }

    case ProductsTypes.UPDATE_PRODUCTS.LOADING:
      return { ...state, updateProductStatus: ActivityStatus.Loading,
        updateProductErorr: null }
    case ProductsTypes.UPDATE_PRODUCTS.SUCCESS:
      return { ...state, updateProductStatus: ActivityStatus.Loaded,
        updateProductErorr: null }
    case ProductsTypes.UPDATE_PRODUCTS.FAILURE:
      return { ...state, updateProductStatus: ActivityStatus.Loaded,
        updateProductErorr: action.payload }
        
    case ProductsTypes.DELETE_PRODUCTS.LOADING:
      return { ...state, deleteProductStatus: ActivityStatus.Loading,
        deleteProductError: null }
    case ProductsTypes.DELETE_PRODUCTS.SUCCESS:
      return { ...state, deleteProductStatus: ActivityStatus.Loaded,
        deleteProductError: null }
    case ProductsTypes.DELETE_PRODUCTS.FAILURE:
      return { ...state, deleteProductStatus: ActivityStatus.Loaded,
        deleteProductError: action.payload }
    default:
      return state;
  }
}

export const productForm = {
  newProductFormValidation: (state, action) => {
    switch(action.type) {
      case ProductsTypes.CREATE_PRODUCTS.SUCCESS: 
        return undefined;       // <--- blow away form data
      default:
        return state;
    }
  }
};