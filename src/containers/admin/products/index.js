import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux';
import { withStyles, Table, TableHead, TableRow, TableBody, TableCell } from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import { Link, Route, Switch } from 'react-router-dom';
import { withSnackbar } from 'notistack';

import ActivityStatus from "@common/enum/activity";
import { productsActions } from './products.actions';
import { formatCurrency } from '@utils/methods';
import Modal from '@components/shared/modals';
import FullScreenLoading from '@components/shared/loading/full-screen';
import UpdateProduct from './update';

const styles = theme => ({
  table: {
    minWidth: 700,
  },
  icon : {
    margin: theme.spacing.unit,
    fontSize: 32,
    cursor: 'pointer',
  },
  productImg: {
    width: '40px',
    height: '40px',
  }
});

const initProductsData = {
  deleteProduct: false,
  deletedProductId: null,
  updateProduct: false,
  selectedProduct: {},
};
const _AdminProducts = ({fetchProducts, productsStore, classes, deleteProducts, match, enqueueSnackbar}) => {
  const [productsData, setProductsData] = useState(initProductsData);
  const [isDeleteSuccess, setIsDeleteSuccess] = useState(false);
  const tmpKey = productsStore.deleteProductStatus;
  useEffect(() => {
    fetchProducts();
    return () => {
      console.log('destroy product list');
    }
  },[]);
  useEffect(() => {
    console.log(productsStore.deleteProductStatus);
    console.log('render list product');
    if (productsStore.deleteProductStatus == ActivityStatus.Loaded) {
      deleteSuccess();
    }
  }, [tmpKey])
  const confirmDeleteProduct = (productId) => {
    setProductsData({...productsData, deleteProduct: true, deletedProductId: productId});
  }
  const deleteProduct = () => {
    deleteProducts(productsData.deletedProductId);
    setProductsData({...productsData, deleteProduct: false, deletedProductId: null});
  }
  const openUpdate = (product) => {
    setProductsData({...productsData, updateProduct: true, selectedProduct: product});
  }
  const deleteSuccess = () => {
    enqueueSnackbar('Xóa sản phẩm thành công', { variant: 'success' });
  }
  return (
    <div>
      <h3>Danh sách sản phẩm</h3>
      <Link to={`${match.path}/new`}>Thêm mới sản phẩm</Link>
      {productsStore.activityStatus === ActivityStatus.Loading && <FullScreenLoading />}
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>STT</TableCell>
            <TableCell>Hình ảnh</TableCell>
            <TableCell >Tên</TableCell>
            <TableCell >Slug</TableCell>
            <TableCell >Mô tả</TableCell>
            <TableCell >Giá</TableCell>
            <TableCell >SL Kho</TableCell>
            <TableCell >Thao tác</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {productsStore.products.map((row, i) => (
            <TableRow key={row._id}>
              <TableCell component="th" scope="row">
                {i}
              </TableCell>
              <TableCell ><img src={row.image} className={classes.productImg}/></TableCell>
              <TableCell >{row.name}</TableCell>
              <TableCell >{row.slug}</TableCell>
              <TableCell >{row.description}</TableCell>
              <TableCell >{formatCurrency(row.price)}</TableCell>
              <TableCell >{row.stock}</TableCell>
              <TableCell >
                <EditIcon className={classes.icon} onClick={() => {openUpdate(row)}} />
                <DeleteForeverIcon className={classes.icon} onClick={() => {confirmDeleteProduct(row._id)}} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Modal type="delete" open={productsData.deleteProduct} onDisagree={() => {setProductsData({...productsData, deleteProduct: false, deletedProductId: null})}} onClose={() => {setProductsData({...productsData, deleteProduct: false, deletedProductId: null})}} onAgree={deleteProduct}/>
      <UpdateProduct open={productsData.updateProduct} title={`Cập nhật sản phẩm ${productsData.selectedProduct.name}`}  onDisagree={() => {setProductsData({...productsData, updateProduct: false})}} onClose={() => {setProductsData({...productsData, updateProduct: false})}} product={productsData.selectedProduct} />
    </div>
  );
}

const mapStateToProps = (rootReducer) => {
  return {
    productsStore: rootReducer.productsReducer,
  };
}

/*const mapDispatchToProps = dispatch => {
  return {
    fetchProducts: () => {
      dispatch(productsActions.fetchProducts())
    },
    deleteProducts: productId => {
      dispatch(productsActions.deleteProducts(productId))
    },
  };
}*/
const mapDispatchToProps = {
  fetchProducts: productsActions.fetchProducts,
  deleteProducts: productsActions.deleteProducts,
}
const AdminProducts = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(_AdminProducts));

export default withSnackbar(AdminProducts);