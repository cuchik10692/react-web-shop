import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import Modal from '@components/shared/modals/index';
import NewProductForm from './form';
import { fileUploadActions } from '@components/shared/file-upload/file-upload.actions';
import { productsActions } from '@containers/admin/products/products.actions';

const UpdateProduct = ({product, updateProduct, ...props}) => {
  const [productS, setProductS] = useState({});
  const handleUpdateProduct = (product) => {
    updateProduct(product);
  }
  // useEffect(() => {
  //   setProductS(product);
  // }, [product._id])
  // const deleteImg = () => {
  //   setProductS({...productS, image: ''});
  // }
  return (
    <Modal {...props}>
      {/* <NewProductForm {...props} initialValues={product} onSubmit={updateProduct} onDeleteImage={deleteImg} /> */}
      <NewProductForm {...props} initialValues={product} onSubmit={handleUpdateProduct} />
    </Modal>
  );
}

const mapDispatchToProps = {
  updateProduct: productsActions.updateProduct,
}

export default connect(null, mapDispatchToProps)(UpdateProduct);
