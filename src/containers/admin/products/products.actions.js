import { RootAction } from "@store/root.actions";
import { ProductsTypes } from './products.types';

export class ProductsActions extends RootAction {

  fetchProducts(param, resolve, reject) {
    return { type: ProductsTypes.FETCH_PRODUCTS.ROOT, payload: param, meta: { resolve, reject } }
  }
  receivedProducts(payload) {
    return { type: ProductsTypes.FETCH_PRODUCTS.SUCCESS, payload }
  }
  failureProducts(payload) {
    return { type: ProductsTypes.FETCH_PRODUCTS.FAILURE, payload }
  }

  createProduct(param, resolve, reject) {
    return { type: ProductsTypes.CREATE_PRODUCTS.ROOT, payload: param, meta: { resolve, reject } }
  }
  createdProduct(payload) {
    return { type: ProductsTypes.CREATE_PRODUCTS.SUCCESS, payload }
  }
  failureCreateProduct(payload) {
    return { type: ProductsTypes.CREATE_PRODUCTS.FAILURE, payload }
  }
  
  updateProduct(param, resolve, reject) {
    return { type: ProductsTypes.UPDATE_PRODUCTS.ROOT, payload: param, meta: { resolve, reject } }
  }
  updatedProduct(payload) {
    return { type: ProductsTypes.UPDATE_PRODUCTS.SUCCESS, payload }
  }
  failureUpdateProduct(payload) {
    return { type: ProductsTypes.UPDATE_PRODUCTS.FAILURE, payload }
  }

  deleteProducts(param, resolve, reject) {
    return { type: ProductsTypes.DELETE_PRODUCTS.ROOT, payload: param, meta: { resolve, reject } }
  }
  deletedProducts(payload) {
    return { type: ProductsTypes.DELETE_PRODUCTS.SUCCESS, payload }
  }
}

export const productsActions = new ProductsActions