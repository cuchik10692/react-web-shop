import { asyncActionType } from '@utils/methods';

export const ProductsTypes = {
  FETCH_PRODUCTS: asyncActionType('FETCH_PRODUCTS'),
  DELETE_PRODUCTS: asyncActionType('DELETE_PRODUCTS'),
  CREATE_PRODUCTS: asyncActionType('CREATE_PRODUCTS'),
  UPDATE_PRODUCTS: asyncActionType('UPDATE_PRODUCTS'),
}