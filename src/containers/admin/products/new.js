import React, { Component, useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { withSnackbar } from 'notistack';

import NewProductForm from './form';
import FullScreenLoading from '@components/shared/loading/full-screen';
import ActivityStatus from '@common/enum/activity';
import { productsActions } from './products.actions';
import { fileUploadActions } from '@components/shared/file-upload/file-upload.actions';


const styles = theme => ({
  new: {
  },
});

class _AdminNewProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    }
    this.addProduct = this.addProduct.bind(this);
  }

  addProduct(product) {
    const {productImages, createProduct} = this.props;
    if (productImages.length > 0) {
      product.image = productImages[0].url;
    } else {
      product.image = 'https://via.placeholder.com/400';
    }
    createProduct(product);
  }

  handleRedirect() {
    return <Redirect to='/admin/products' />
  }

  componentDidUpdate(prevProps, prevState) {
    const { addProductStatus, enqueueSnackbar, createProductErorr } = this.props;
    if (prevProps.addProductStatus !== addProductStatus &&  addProductStatus == ActivityStatus.Loaded) {
      if (!createProductErorr) {
        enqueueSnackbar('Tạo sản phẩm thành công', { variant: 'success' });
        this.setState({
          redirect: true,
        })
      } else {
        enqueueSnackbar('Tạo sản phẩm không thành công. Vui lòng thử lại!', { variant: 'error' });
      }
    }
  }
  
  render() {
    const { redirect } = this.state;
    const { classes, addProductStatus, uploadImageStatus } = this.props;
    return (
      <div className={classes.new}>
      {addProductStatus === ActivityStatus.Loading && <FullScreenLoading />}
      {redirect && this.handleRedirect()}
      <NewProductForm onSubmit={this.addProduct} uploadImageStatus={uploadImageStatus} />
    </div>
    );
  }
}


const mapStateToProps = (rootReducer) => {
  return { 
    addProductStatus: rootReducer.productsReducer.addProductStatus,
    createProductErorr: rootReducer.productsReducer.createProductErorr,
    productImages: rootReducer.fileUploadReducer.images,
    uploadImageStatus: rootReducer.fileUploadReducer.activityStatus,
  };
}
const mapDispatchToProps = {
  createProduct: productsActions.createProduct,
}
const AdminNewProduct = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(_AdminNewProduct));

export default withSnackbar(AdminNewProduct);