import { BaseService } from "@service/base.service";
import { AdminUrl } from '@common/urls/admin';

export class ProductsService extends BaseService {
  /**
   *
   */
  constructor(path) {
    super(path);
  }

  /**
   * Fetch list product
   * @param param 
   */
  fetchProducts(param) {
    return super.select(AdminUrl.Products)
      .then((response) => {
       return response
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

  createProduct(data) {
    return super.post(AdminUrl.Products, data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

  updateProduct(data) {
    return super.put(AdminUrl.Products + '/' + data._id, data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

  deleteProducts(param) {
    return super.delete(AdminUrl.Products + '/' + param)
      .then((response) => {
        return response
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

}