import { takeLatest, put } from "redux-saga/effects";
import ActivityStatus from '@common/enum/activity';
import { ProductsService } from './products.service';
import { productsActions } from './products.actions';
import { ProductsTypes } from './products.types';
import { fileUploadActions } from '@components/shared/file-upload/file-upload.actions';

const service = new ProductsService

export function* fetchProducts(action) {
  try {
    yield put(productsActions.loadingActivity(ActivityStatus.Loading,
      ProductsTypes.FETCH_PRODUCTS.LOADING))

    const result = yield service.fetchProducts(action.payload);
    yield put(productsActions.receivedProducts(result))
  } catch(ex) {
    console.log('error');
    yield put(productsActions.failureProducts());
  }
}
export function* watchFetchProducts() {
  yield takeLatest(ProductsTypes.FETCH_PRODUCTS.ROOT, fetchProducts)
}

export function* createProduct(action) {
  try {
    yield put(productsActions.loadingActivity(ActivityStatus.Loading,
      ProductsTypes.CREATE_PRODUCTS.LOADING))

    const product = yield service.createProduct(action.payload);
    if (product.error) {
      yield put(productsActions.failureCreateProduct(true));
    } else {
      yield put(productsActions.createdProduct(product));
      // reset uploaded image
      yield put(fileUploadActions.deletedImage());
    }
  } catch(ex) {
    yield put(productsActions.failureCreateProduct(true));
    console.log('error');
  }
}
export function* watchCreateProduct() {
  yield takeLatest(ProductsTypes.CREATE_PRODUCTS.ROOT, createProduct)
}

export function* updateProduct(action) {
  try {
    yield put(productsActions.loadingActivity(ActivityStatus.Loading,
      ProductsTypes.UPDATE_PRODUCTS.LOADING))
    debugger;
    const product = yield service.updateProduct(action.payload);
    if (product.error) {
      yield put(productsActions.failureUpdateProduct(true));
    } else {
      yield put(productsActions.updatedProduct(product));
    }
  } catch(ex) {
    yield put(productsActions.failureCreateProduct(true));
    console.log('error');
  }
}
export function* watchUpdateProduct() {
  yield takeLatest(ProductsTypes.UPDATE_PRODUCTS.ROOT, updateProduct)
}

export function* deleteProducts(action) {
  try {
    yield put(productsActions.loadingActivity(ActivityStatus.Loading,
      ProductsTypes.DELETE_PRODUCTS.LOADING))
    const result = yield service.deleteProducts(action.payload);
    yield put(productsActions.deletedProducts(result));
    yield put(productsActions.loadingActivity(null, ProductsTypes.FETCH_PRODUCTS.ROOT));
  } catch(ex) {
    console.log('error');
  }
}
export function* watchDeleteProducts() {
  yield takeLatest(ProductsTypes.DELETE_PRODUCTS.ROOT, deleteProducts)
}
