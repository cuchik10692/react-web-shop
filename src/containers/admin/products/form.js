import React, {useState, useEffect} from 'react';

import { bindActionCreators } from 'redux';
import { Field, reduxForm, change } from 'redux-form'
import { withStyles, FormControl, InputLabel, Input, FormGroup, Button, FormHelperText } from '@material-ui/core';
import FileUpload from '@components/shared/file-upload/images';
import validate from './validate';
import BaseInputBlock from '@components/blocks/inputs/base';
import ActivityStatus from '@common/enum/activity';

const styles = theme => ({
  new: {
  },
  new__form : {
  },
  new__form_group: {
    margin: theme.spacing.unit
  },
  button: {
    margin: theme.spacing.unit,
    marginLeft: 0
  }
});

let NewProductForm = ({classes, handleSubmit, pristine, reset, submitting, uploadImageStatus, onDeleteImage}) => {
  const resetForm = () => {
    reset();
  }

  return (
    <form className={classes.new__form} onSubmit={handleSubmit} encType="multipart/form-data">
      <FormGroup row className={classes.new__form_group}>
          <Field
            name="name"
            component={BaseInputBlock}
            label="Tên sản phẩm"
          />
      </FormGroup>
      <FormGroup row className={classes.new__form_group}>
        <Field
          name="image"
          component={FileUpload}
          label="Hình ảnh"
          type="file"
          onDeleteImage={onDeleteImage}
        />
      </FormGroup>
      <FormGroup row className={classes.new__form_group}>
        <Field
          name="description"
          component={BaseInputBlock}
          label="Mô tả"
          multiline
        />
      </FormGroup>
      <FormGroup row className={classes.new__form_group}>
        <Field
          name="price"
          component={BaseInputBlock}
          label="Giá"
          type="number"
        />
      </FormGroup>
      <FormGroup row className={classes.new__form_group}>
        <Field
          name="stock"
          component={BaseInputBlock}
          label="SL Kho"
        />
      </FormGroup>
      <FormGroup row className={classes.new__form_group}>
          <Button type="button" variant="contained" disabled={pristine || submitting || uploadImageStatus == ActivityStatus.Loading} onClick={resetForm} className={classes.button}>Nhập lại</Button>
          <Button type="submit" variant="contained" color="primary" disabled={pristine || submitting || uploadImageStatus == ActivityStatus.Loading} className={classes.button}>Thêm mới</Button>
      </FormGroup>
    </form>
  );
}

export default reduxForm({
  form: 'newProductFormValidation',
  validate,
  enableReinitialize: true,
})(withStyles(styles)(NewProductForm));