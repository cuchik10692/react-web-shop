import ActivityStatus from "@common/enum/activity";

export class ProductsState {
  /**
   *
   */
  activityStatus;
  products;
  error;
  addProductStatus;
  deleteProductStatus;
  updateProductStatus;

  constructor() {
    this.products = [];
    this.activityStatus = ActivityStatus.NoActivity;
    this.addProductStatus = ActivityStatus.NoActivity;
    this.deleteProductStatus = ActivityStatus.NoActivity;
    this.updateProductStatus = ActivityStatus.NoActivity;
    this.error = null;
  }

}