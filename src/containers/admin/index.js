import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Hidden from '@material-ui/core/Hidden';
import Navigator from '@components/layouts/nav';
import Header from '@components/layouts/header';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AdminCategories from '@containers/admin/categories';
import AdminDashboard from '@containers/admin/dashboard';
import AdminProducts from '@containers/admin/products';
import AdminNewProduct from '@containers/admin/products/new';
import GetCurrentUser from '@containers/auth/current-user/index';

let theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    h5: {
      fontWeight: 500,
      fontSize: 26,
      letterSpacing: 0.5,
    },
  },
  palette: {
    primary: {
      light: '#63ccff',
      main: '#009be5',
      dark: '#006db3',
    },
  },
  shape: {
    borderRadius: 8,
  },
});

theme = {
  ...theme,
  overrides: {
    MuiDrawer: {
      paper: {
        backgroundColor: '#18202c',
      },
    },
    MuiButton: {
      label: {
        textTransform: 'initial',
      },
      contained: {
        boxShadow: 'none',
        '&:active': {
          boxShadow: 'none',
        },
      },
    },
    MuiTabs: {
      root: {
        marginLeft: theme.spacing.unit,
      },
      indicator: {
        height: 3,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        backgroundColor: theme.palette.common.white,
      },
    },
    MuiTab: {
      root: {
        textTransform: 'initial',
        margin: '0 16px',
        minWidth: 0,
        [theme.breakpoints.up('md')]: {
          minWidth: 0,
        },
      },
      labelContainer: {
        padding: 0,
        [theme.breakpoints.up('md')]: {
          padding: 0,
        },
      },
    },
    MuiIconButton: {
      root: {
        padding: theme.spacing.unit,
      },
    },
    MuiTooltip: {
      tooltip: {
        borderRadius: 4,
      },
    },
    MuiDivider: {
      root: {
        backgroundColor: '#404854',
      },
    },
    MuiListItemText: {
      primary: {
        fontWeight: theme.typography.fontWeightMedium,
      },
    },
    MuiListItemIcon: {
      root: {
        color: 'inherit',
        marginRight: 0,
        '& svg': {
          fontSize: 20,
        },
      },
    },
    MuiAvatar: {
      root: {
        width: 32,
        height: 32,
      },
    },
  },
  props: {
    MuiTab: {
      disableRipple: true,
    },
  },
  mixins: {
    ...theme.mixins,
    toolbar: {
      minHeight: 48,
    },
  },
};

const styles = {
  root: {
    display: 'flex',
    minHeight: '100vh',
  },
  appContent: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  mainContent: {
    flex: 1,
    padding: '48px 36px 0',
    background: '#eaeff1',
  },
};
class AdminPage extends React.Component {
  
  render() {
    const { classes } = this.props;
    return (
      <Router>
        <MuiThemeProvider theme={theme}>
          <GetCurrentUser />
          <div className={classes.root}>
            <CssBaseline />
            <Navigator />
            <div className={classes.appContent}>
              <Header />
              <main className={classes.mainContent}>
                <div className="">
                <Route
                    path="/admin"
                    render={({ match: { url } }) => (
                      <>
                        <Route exact path={`${url}`} component={AdminDashboard} />
                        <Route path={`${url}/categories`} component={AdminCategories}/>
                        <Route path={`${url}/products`} render={({match: {url}}) => (
                          <>
                            <Route exact path={`${url}`} component={AdminProducts} />
                            <Route exact path={`${url}/new`} component={AdminNewProduct} />
                          </>
                        )} />
                      </>
                    )}
                  /> 
                </div>
              </main>
            </div>
          </div>
        </MuiThemeProvider>
      </Router>
    );
  }
}

AdminPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdminPage);

// export default connect(
//   state => ({
//     list: state.templates.list,
//     loading: state.templates.loading,
//     error: state.templates.fetchError,
//     token: state.templates.continuationToken,
//     segmentType: state.appConfigs.segmentType,
//     search: state.templates.search,
//   }),
//   {
//     loadList: templateActions.loadTemplateList,
//     deleteTemplate: templateActions.deleteTemplate,
//     filterTemplate: templateActions.filterTemplate,
//     changeSearchValue: templateActions.changeSearchValue,
//     searchTemplate: templateActions.searchTemplate,
//     resetSearchValue: templateActions.resetSearchValue,
//   },
// )(ListPage);

// <Switch>
//         <Route exact key="widget" path="/widget" component={SegpWidget} />
//         <Route
//           exact
//           key="login"
//           path="/login"
//           component={props =>
//             renderAsync(() => import('./Login/'), props, false)
//           }
//         />
//         <Route
//           render={() => (
//             <Layout
//               className={classnames(
//                 styles.layout,
//                 className,
//                 topBanner ? styles.layoutNotify : '',
//               )}
//             >
//               <AppBar />
//               {topBanner && (
//                 <Row className={classnames(styles.notification)}>
//                   {topBanner}
//                 </Row>
//               )}
//               <Layout hasSider="true">
//                 <AppSider />
//                 <AppContent />
//               </Layout>
//             </Layout>
//           )}
//         />
//       </Switch>