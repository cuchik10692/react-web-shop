import React, { useEffect } from 'react';
import { Field, reduxForm, untouch } from 'redux-form';
import { withStyles, FormControl, InputLabel, Input, FormGroup, Button, FormHelperText } from '@material-ui/core';
import { Link } from 'react-router-dom';

import validate from './validate';
import BaseInputBlock from '@components/blocks/inputs/base';

const styles = theme => ({
  register__form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  register__form__submit: {
    marginTop: theme.spacing.unit * 3,
  },
  register__form__reset: {
    marginTop: theme.spacing.unit * 2,
  },
  register__form__group: {
    marginTop: theme.spacing.unit * 2,
  }
});
const _RegisterForm = ({classes, handleSubmit, pristine, reset, submitting, untouch}) => {
  useEffect(() => {
    untouch('username', 'password', 'confirmation_password', 'email', 'fullname');
  }, []);
  return (
    <>
      <form className={classes.register__form} onSubmit={handleSubmit}>
      <FormControl margin="normal" className={classes.register__form_group} required fullWidth>
          <Field
            name="username"
            component={BaseInputBlock}
            label="Tên đăng nhập"
          />
      </FormControl>
      <FormControl margin="normal" className={classes.register__form_group} required fullWidth>
          <Field
            name="fullname"
            component={BaseInputBlock}
            label="Họ và tên"
          />
      </FormControl>
      <FormControl margin="normal" className={classes.register__form_group} required fullWidth>
          <Field
            name="password"
            component={BaseInputBlock}
            label="Mật khẩu"
            type="password"
          />
      </FormControl>
      <FormControl margin="normal" className={classes.register__form_group} required fullWidth>
          <Field
            name="confirmation_password"
            component={BaseInputBlock}
            label="Xác nhận mật khẩu"
            type="password"
          />
      </FormControl>
      <FormControl margin="normal" className={classes.register__form_group} required fullWidth>
          <Field
            name="email"
            component={BaseInputBlock}
            label="Email"
            type="text"
          />
      </FormControl>
      <FormGroup row className={classes.register__form__group}>
        <span>Nếu bạn đã có tài khoản, vui lòng </span> &nbsp;
        <Link to="/login"> đăng nhập</Link>
      </FormGroup>
      <Button type="submit" variant="contained" color="primary" disabled={pristine || submitting} className={classes.register__form__submit} fullWidth>Đăng ký</Button>
      <Button type="button" variant="contained" disabled={pristine || submitting} onClick={reset} className={classes.register__form__reset} fullWidth>Nhập lại</Button>
    </form>
    </>
  );
}

const LoginForm = reduxForm({
  form: 'registerFormValidation',
  validate,
}, untouch)(withStyles(styles)(_RegisterForm));

export default LoginForm;