import React, { useEffect } from 'react';
import  { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';

import { authActions } from '../auth.actions';
import RegisterForm from './form';
import AuthPageTemplate from '../template';

const styles = theme => ({
  register: {

  }
});
const _Register = ({classes, register, authStore}) => {
  const handleSubmit = (auth) => {
    register(auth);
  }
  return (
    <AuthPageTemplate title={'Đăng ký'}>
      <RegisterForm onSubmit={handleSubmit} />
    </AuthPageTemplate>
  );
}

const mapStateToProps = (rootReducer) => {
  return { authStore: rootReducer.authReducer };
}

const mapDispatchToProps = {
  register: authActions.register,
};

const Register = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(_Register));

export default Register;