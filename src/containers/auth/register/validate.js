import { required, checkLength, checkConfirmation, checkEmail } from '@utils/validates';

const validate = values => {
  let errors = {}
  const requiredFields = [
    'username',
    'password',
    'confirmation_password',
    'email',
  ]
  const lengthFields = [
    {field: 'username', limit: 255, compare: 'max'},
    {field: 'username', limit: 5, compare: 'min'},
    {field: 'password', limit: 255, compare: 'max'},
    {field: 'password', limit: 5, compare: 'min'},
    {field: 'confirmation_password', limit: 255, compare: 'max'},
    {field: 'confirmation_password', limit: 5, compare: 'min'},
    {field: 'email', limit: 255, compare: 'max'},
    {field: 'fullname', limit: 255, compare: 'max'},
  ]
  const confirmationCheckObj = {
    field: 'password',
    confirmationField: 'confirmation_password',
    errorMsg: 'Xác nhận mật khẩu không đúng',
  };
  const emailObj = {
    field: 'email',
    errorMsg: 'Email không đúng định dạng',
  }


  const requireError = required(requiredFields, values);
  const maxError = checkLength(lengthFields, values);
  const confirmationError = checkConfirmation(confirmationCheckObj, values);
  const emailEror = checkEmail(emailObj, values);

  errors = {...errors, ...requireError, ...maxError, ...confirmationError, ...emailEror};
  console.log(errors);
  return errors;
}

export default validate;