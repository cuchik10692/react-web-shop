import { takeLatest, put } from "redux-saga/effects";
import ActivityStatus from '@common/enum/activity';
import { AuthService } from './auth.service';
import { authActions } from './auth.actions';
import { AuthTypes } from './auth.types';

const service = new AuthService

export function* login(action) {
  try {
    yield put(authActions.loadingActivity(ActivityStatus.Loading,
      AuthTypes.LOGIN.LOADING))

    const result = yield service.login(action.payload);
    yield put(authActions.loginSuccess(result))
  } catch(ex) {
    yield put(authActions.loginFailure());
  }
}
export function* watchLogin() {
  yield takeLatest(AuthTypes.LOGIN.ROOT, login)
}

export function* register(action) {
  try {
    yield put(authActions.loadingActivity(ActivityStatus.Loading,
      AuthTypes.REGISTER.LOADING))

    const result = yield service.register(action.payload);
    yield put(authActions.registerSuccess(result))
  } catch(ex) {
    console.log('error');
    yield put(authActions.registerFailure());
  }
}
export function* watchRegister() {
  yield takeLatest(AuthTypes.REGISTER.ROOT, register)
}

export function* currentUser(action) {
  try {
    yield put(authActions.loadingActivity(ActivityStatus.Loading,
      AuthTypes.CURRENT_USER.LOADING))

    const result = yield service.currentUser(action.payload);
    yield put(authActions.currentUserSuccess(result))
  } catch(ex) {
    console.log('error');
    yield put(authActions.currentUserFailure());
  }
}
export function* watchCurrentUser() {
  yield takeLatest(AuthTypes.CURRENT_USER.ROOT, currentUser)
}
