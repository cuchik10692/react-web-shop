import { asyncActionType } from '@utils/methods';

export const AuthTypes = {
  LOGIN: asyncActionType('LOGIN'),
  REGISTER: asyncActionType('REGISTER'),
  CURRENT_USER: asyncActionType('CURRENT_USER'),
  LOGOUT: 'RESET',
}