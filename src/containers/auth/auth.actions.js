import { RootAction } from "@store/root.actions";
import { AuthTypes } from './auth.types';

export class AuthActions extends RootAction {

  login(param, resolve, reject) {
    return { type: AuthTypes.LOGIN.ROOT, payload: param, meta: { resolve, reject } }
  }
  loginSuccess(payload) {
    return { type: AuthTypes.LOGIN.SUCCESS, payload }
  }
  loginFailure(payload) {
    return { type: AuthTypes.LOGIN.FAILURE, payload }
  }

  register(param, resolve, reject) {
    return { type: AuthTypes.REGISTER.ROOT, payload: param, meta: { resolve, reject } }
  }
  registerSuccess(payload) {
    return { type: AuthTypes.REGISTER.SUCCESS, payload }
  }
  registerFailure(payload) {
    return { type: AuthTypes.REGISTER.FAILURE, payload }
  }

  currentUser(param, resolve, reject) {
    return { type: AuthTypes.CURRENT_USER.ROOT, payload: param, meta: { resolve, reject } }
  }
  currentUserSuccess(payload) {
    return { type: AuthTypes.CURRENT_USER.SUCCESS, payload }
  }
  currentUserFailure(payload) {
    return { type: AuthTypes.CURRENT_USER.FAILURE, payload }
  }

  logout(param, resolve, reject) {
    return { type: AuthTypes.LOGOUT, payload: param, meta: { resolve, reject } }
  }
}

export const authActions = new AuthActions