import { BaseService } from "@service/base.service";
import { AuthUrl } from '@common/urls/auth';

export class AuthService extends BaseService {
  /**
   *
   */
  constructor(path) {
    super(path);
  }


  login(data) {
    return super.post(AuthUrl.Login, data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

  register(data) {
    return super.post(AuthUrl.Register, data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

  currentUser(data) {
    return super.select(AuthUrl.CurrentUser)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

}