import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { authActions } from '../auth.actions';
import ActivityStatus from '@common/enum/activity';

class GetCurrentUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    }
    this.redirectToLogin = this.redirectToLogin.bind(this);
  }

  componentDidMount(a, b, c) {
    const { token, getCurrentUser } = this.props;
    if (token) {
      getCurrentUser();
    }
  };

  componentDidUpdate(prevProps) {
    const { currentUserStatus, currentUserError } = this.props;
    if (currentUserStatus !== prevProps.currentUserStatus && currentUserStatus == ActivityStatus.Loaded && currentUserError) {
      this.setState({
        redirect: true,
      })
    }
  }

  redirectToLogin() {
    return (<Redirect to="/login" />);
  }

  render() {
    const {redirect} = this.state;
    return (
      <>
        {redirect && this.redirectToLogin()}
      </>
    ); 
  }
}

export default connect(
  state => ({
    token: state.authReducer.token,
    currentUserStatus: state.authReducer.currentUserStatus,
    currentUserError: state.authReducer.currentUserError,
  }),
  {
    getCurrentUser: authActions.currentUser,
  },
)(GetCurrentUser);