import React, { useEffect } from 'react';

import { Field, reduxForm, untouch } from 'redux-form';
import { Link } from 'react-router-dom';
import { withStyles, FormControl, InputLabel, Input, FormGroup, Button, FormHelperText, FormControlLabel, Checkbox } from '@material-ui/core';
import validate from './validate';
import BaseInputBlock from '@components/blocks/inputs/base';

const styles = theme => ({
  login__form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  login__form__submit: {
    marginTop: theme.spacing.unit * 3,
  },
});
const _LoginForm = ({classes, handleSubmit, pristine, reset, submitting, untouch}) => {
  useEffect(() => {
    untouch('username', 'password');
  }, []);
  return (
    <>
      <form className={classes.login__form} onSubmit={handleSubmit}>
        <FormControl  margin="normal" className={classes.login__form_group} required fullWidth>
          <Field
            name="username"
            component={BaseInputBlock}
            label="Tên đăng nhập"
            autoFocus
          />
        </FormControl >
        <FormControl  margin="normal" className={classes.login__form_group} required fullWidth>
          <Field
            name="password"
            component={BaseInputBlock}
            label="Mật khẩu"
            type="password"
          />
        </FormControl >
        <FormControl>
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Lưu mật khẩu"
          />
        </FormControl>
        <FormGroup row>
          <span>Nếu bạn chưa có tài khoản, vui lòng </span> &nbsp;
          <Link to="/register"> đăng ký</Link>
        </FormGroup>
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.login__form__submit}
            disabled={pristine || submitting}
          >
          Đăng nhập
        </Button>
    </form>
    </>
  );
}

const LoginForm = reduxForm({
  form: 'loginFormValidation',
  validate,
}, untouch)(withStyles(styles)(_LoginForm));

export default LoginForm;