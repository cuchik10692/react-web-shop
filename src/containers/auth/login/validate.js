import { required, checkLength } from '@utils/validates';

const validate = values => {
  let errors = {}
  const requiredFields = [
    'username',
    'password',
  ]
  const lengthFields = [
    {field: 'username', limit: 255, compare: 'max'},
    {field: 'username', limit: 5, compare: 'min'},
    {field: 'password', limit: 255, compare: 'max'},
    {field: 'password', limit: 5, compare: 'min'},
  ]
  const requireError = required(requiredFields, values);
  const maxError = checkLength(lengthFields, values);
  errors = {...errors, ...requireError, ...maxError};
  return errors;
}

export default validate;