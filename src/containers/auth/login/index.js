import React, { Component } from 'react';
import  { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withSnackbar } from 'notistack';

import { withStyles} from '@material-ui/core';
import LoginForm from './form';
import { authActions } from '../auth.actions';
import AuthPageTemplate from '../template';
import ActivityStatus from '@common/enum/activity';
import FullScreenLoading from '@components/shared/loading/full-screen';

const styles = theme => ({
});

class _Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkLogin = this.checkLogin.bind(this);
  }

  handleSubmit(auth) {
    const { login } = this.props;
    login(auth);
  }
  redirectAfterLogin() {
    return <Redirect to='/admin' />;
  }
  checkLogin(pushNotification) {
    const { loginStatus, loginError, enqueueSnackbar, token } = this.props;
    if (loginStatus === ActivityStatus.Loaded) {
      if (token) {
        this.setState({
          redirect: true,
        })
      } else {
        if (pushNotification) {
          enqueueSnackbar('Đăng nhập không thành công', { variant: 'error' });
        }
      }
    }
  }
  componentDidMount() {
    this.checkLogin();
  }
  componentDidUpdate(prevProps) {
    this.checkLogin(true);
  }

  render() {
    const { classes, loginStatus } = this.props;
    const { redirect } = this.state;
    return (
      <AuthPageTemplate title={'Đăng nhập'}>
        {redirect && this.redirectAfterLogin()}
        {loginStatus == ActivityStatus.Loading && <FullScreenLoading />}
        <LoginForm onSubmit={this.handleSubmit} classes={classes}/>
      </AuthPageTemplate>
    );
  }
}

const mapStateToProps = (rootReducer) => {
  return {
    loginStatus: rootReducer.authReducer.loginStatus,
    loginError: rootReducer.authReducer.loginError,
    token: rootReducer.authReducer.token,
  };
}

const mapDispatchToProps = {
  login: authActions.login,
};

const Login = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(_Login));
export default withSnackbar(Login);