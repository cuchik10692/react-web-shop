import ActivityStatus from "@common/enum/activity";

export class AuthState {
  /**
   *
   */
  loginStatus;
  token;
  loginError;
  
  currentUserStatus;
  currentUser;
  currentUserError;

  constructor() {
    this.token = '';
    this.currentUser = {};
    this.loginStatus = ActivityStatus.NoActivity;
    this.currentUserStatus = ActivityStatus.NoActivity;
    this.loginError = null;
    this.currentUserError = null;
  }

}