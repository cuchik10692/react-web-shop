import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form';

import { AuthState } from './auth.state'
import { AuthTypes } from './auth.types';
import ActivityStatus from "@common/enum/activity";

const initialAuthState = new AuthState();

export const authReducer = (state = initialAuthState, action) => {
  switch (action.type) {
    case AuthTypes.LOGIN.LOADING:
      return { ...state, token: '',
      loginStatus: ActivityStatus.Loading,
      loginError: null }
    case AuthTypes.LOGIN.SUCCESS:
      const token = (action.payload) ? action.payload.access_token : '';
      return { ...state, token,
        loginStatus: ActivityStatus.Loaded,
        loginError: null }
    case AuthTypes.LOGIN.FAILURE:
      return { ...state, loginStatus: ActivityStatus.Loaded,
        loginError: true }
    case AuthTypes.CURRENT_USER.LOADING:
      return { ...state, currentUser: {},
      currentUserStatus: ActivityStatus.Loading,
        currentUserError: null }
    case AuthTypes.CURRENT_USER.SUCCESS:
      return { ...state, currentUser: action.payload,
        currentUserStatus: ActivityStatus.Loaded,
        currentUserError: null }
    case AuthTypes.CURRENT_USER.FAILURE:
      return { ...state, currentUserStatus: ActivityStatus.Loaded,
        currentUserError: null }
    default:
      return state;
  }
}

export const authForm = {
  loginFormValidation: (state, action) => {
    switch(action.type) {
      case AuthTypes.LOGIN.SUCCESS: 
        return undefined;       // <--- blow away form data
      default:
        return state;
    }
  }
};