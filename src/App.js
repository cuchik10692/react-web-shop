import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { connect } from 'react-redux';

import HomePage from '@containers/front-end/home-page';
import AdminPage from '@containers/admin';
import Login from '@containers/auth/login';
import Register from '@containers/auth/register';
import PrivateRoute from '@components/shared/private';

import './App.css';

class App extends Component {

  
  render() {
    return (
      <>
        <Router>
          <PrivateRoute path="/" exact component={HomePage} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <PrivateRoute path="/admin" component={AdminPage} />
          <PrivateRoute path="/home" component={HomePage} />
        </Router>
      </>
    );
  }
}

export default App;
