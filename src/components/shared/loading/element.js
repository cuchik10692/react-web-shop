import React from 'react';
import { withStyles, CircularProgress } from '@material-ui/core';
const styles = theme => ({
  loading__icon: {
  }
});
const ElementLoading = ({classes, ...props}) => {
  return (
    <CircularProgress className={classes.loading__icon} {...props} />
  );
}

export default withStyles(styles)(ElementLoading);