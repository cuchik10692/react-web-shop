import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const _PrivateRoute = ({ component: Component, token, ...rest }) => (
    <Route {...rest} render={props => (
        token 
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)

const mapStateToProps = (rootReducer) => {
  return { token: rootReducer.authReducer.token };
}

const PrivateRoute = connect(mapStateToProps, null)(_PrivateRoute);
export default PrivateRoute;