import { takeLatest, put, delay } from "redux-saga/effects";
import ActivityStatus from '@common/enum/activity';
import { FileUploadService } from './file-upload.service';
import { fileUploadActions } from './file-upload.actions';
import { FileUploadTypes } from './file-upload.types';

const service = new FileUploadService

export function* uploadImage(action) {
  try {
    yield put(fileUploadActions.loadingActivity(ActivityStatus.Loading,
      FileUploadTypes.UPLOAD_IMAGE.LOADING))

    const result = yield service.uploadImage(action.payload);

    yield put(fileUploadActions.uploadedImage(result))
  } catch(ex) {
    console.log('error');
    yield put(fileUploadActions.failureUploadImage());
  }
}
export function* watchUploadImage() {
  yield takeLatest(FileUploadTypes.UPLOAD_IMAGE.ROOT, uploadImage)
}

export function* deleteImage(action) {
  yield put(fileUploadActions.loadingActivity(ActivityStatus.Loading,
    FileUploadTypes.DELETE_IMAGE.LOADING))
    yield delay(1);
  const result = yield service.deleteImage(action.payload);

  yield put(fileUploadActions.deletedImage(result));
}
export function* watchDeleteImage() {
  yield takeLatest(FileUploadTypes.DELETE_IMAGE.ROOT, deleteImage)
}