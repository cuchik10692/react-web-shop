import { BaseService } from "@service/base.service";
import { FileUploadUrl } from '@common/urls/file-upload';

export class FileUploadService extends BaseService {
  /**
   *
   */
  constructor(path) {
    super(path);
  }

  uploadImage(data) {
    return super.post(FileUploadUrl.Image, data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error)
      })
  }

  deleteImage() {
    return [];
  }

}