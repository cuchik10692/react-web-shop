import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';

import { Input } from '@material-ui/core';
import { fileUploadActions } from './file-upload.actions';
import ElementLoading from '@components/shared/loading/element';
import ActivityStatus from '@common/enum/activity';
import ImageDisplay from './image-display/index';

const styles = theme => ({
});

class _FileUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUrl: '',
    }
    this.handleUploadFile = this.handleUploadFile.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleUploadFile(event) {
    const { uploadImage } = this.props;
    const file = event.target.files[0];
    if (file) {
      const data = new FormData();
      data.append('file', file);
      uploadImage(data);
    }
  }

  handleDelete() {
    const { deleteImage, onDeleteImage } = this.props;
    document.getElementById('file_upload').value = '';
    deleteImage();
    if (onDeleteImage) {
      onDeleteImage();
    }
  }

  componentDidMount() {
    const { meta: { initial  } } = this.props;
    this.setState({
      imageUrl: initial,
    })
  }

  componentDidUpdate(prevProps) {
    const { imageStore } = this.props;
    const { imageStore: oldImageStore } = prevProps;
    if (oldImageStore.activityStatus !== imageStore.activityStatus && imageStore.activityStatus == ActivityStatus.Loaded) {
      if (imageStore.images.length > 0) {
        this.setState({
          imageUrl: imageStore.images[0].url,
        })
      } else {
        this.setState({
          imageUrl: '',
        })
      }
    }
  }

  render() {
    const { imageStore, url, meta: { initial  } } = this.props;
    const { imageUrl } = this.state;
    return (
      <>
        <Input type="file" onChange={this.handleUploadFile} disabled={imageStore.activityStatus == ActivityStatus.Loading} id="file_upload"/>
        {imageUrl && <ImageDisplay url={imageUrl} onDelete={this.handleDelete} />}
        {imageStore.activityStatus == ActivityStatus.Loading && <ElementLoading size={30}/>}
      </>
    );
  }
}

const mapStateToProps = (rootReducer) => {
  return { imageStore: rootReducer.fileUploadReducer };
}

const FileUpload = connect(mapStateToProps, {
  uploadImage: fileUploadActions.uploadImage,
  deleteImage: fileUploadActions.deleteImage,
})(withStyles(styles)(_FileUpload));

export default FileUpload;