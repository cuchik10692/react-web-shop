import { FileUploadState } from './file-upload.state'
import { FileUploadTypes } from './file-upload.types';
import ActivityStatus from "@common/enum/activity";

const initialFileUploadState = new FileUploadState();

export const fileUploadReducer =
  (state = initialFileUploadState, action) => {
    switch (action.type) {
      case FileUploadTypes.UPLOAD_IMAGE.LOADING:
        return { ...state, images: [],
          activityStatus: ActivityStatus.Loading,
          error: null }
      case FileUploadTypes.UPLOAD_IMAGE.SUCCESS:
        return { ...state, images: action.payload,
          activityStatus: ActivityStatus.Loaded,
          error: null }
      case FileUploadTypes.UPLOAD_IMAGE.FAILURE:
        return { ...state, activityStatus: ActivityStatus.Loaded,
          error: null }
      case FileUploadTypes.DELETE_IMAGE.LOADING:
        return { ...state, images: [],
          activityStatus: ActivityStatus.Loading,
          error: null }
      case FileUploadTypes.DELETE_IMAGE.SUCCESS:
        return { ...state, images: [],
          activityStatus: ActivityStatus.Loaded,
          error: null }
      default:
        return state;
    }

  }