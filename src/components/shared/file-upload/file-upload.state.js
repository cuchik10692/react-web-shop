import ActivityStatus from "@common/enum/activity";

export class FileUploadState {
  /**
   *
   */
  activityStatus;
  images;
  error;

  constructor() {
    this.images = [];
    this.activityStatus = ActivityStatus.NoActivity;
    this.error = null;
  }

}