import { RootAction } from "@store/root.actions";
import { FileUploadTypes } from './file-upload.types';

export class FileUploadActions extends RootAction {

  uploadImage(param, resolve, reject) {
    return { type: FileUploadTypes.UPLOAD_IMAGE.ROOT, payload: param, meta: { resolve, reject } }
  }
  uploadedImage(payload) {
    return { type: FileUploadTypes.UPLOAD_IMAGE.SUCCESS, payload }
  }
  failureUploadImage(payload) {
    return { type: FileUploadTypes.UPLOAD_IMAGE.FAILURE, payload }
  }

  deleteImage(param, resolve, reject) {
    return { type: FileUploadTypes.DELETE_IMAGE.ROOT, payload: param, meta: { resolve, reject } }
  }
  deletedImage(payload) {
    return { type: FileUploadTypes.DELETE_IMAGE.SUCCESS, payload }
  }
}

export const fileUploadActions = new FileUploadActions