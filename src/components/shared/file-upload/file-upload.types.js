import { asyncActionType } from '@utils/methods';

export const FileUploadTypes = {
  UPLOAD_IMAGE: asyncActionType('UPLOAD_IMAGE'),
  DELETE_IMAGE: asyncActionType('DELETE_IMAGE'),
}