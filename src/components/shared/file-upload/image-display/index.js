import React from 'react';

import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { IconButton } from '@material-ui/core';

const ImageDisplay = ({url, onDelete}) => {
  const handleClick = () => {
    onDelete();
  }
  return (
    <div>
      <IconButton onClick={handleClick}>
        <HighlightOffIcon />
      </IconButton>
      <img src={url} />
    </div>
  );
}

export default ImageDisplay;