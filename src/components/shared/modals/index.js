import React from 'react';

import ModalDelete from './delete';
import ModalDefault from './default';

const Modal = ({type, ...other}) => {
  let modal = <ModalDefault {...other} />
  if (type === 'delete') {
    modal = <ModalDelete {...other} />
  }
  return modal;
}

export default Modal;