import React from 'react';
import { Dialog, DialogTitle } from '@material-ui/core';

const ModalDefault = ({children, open = false, onClose, fullWidth = true, title, maxWidth = 'sm'}) => {
  return (
    <Dialog
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          open={open}
          onClose={onClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{title ? title : 'Default modal'}</DialogTitle>
          {children}
        </Dialog>
  );
}

export default ModalDefault;