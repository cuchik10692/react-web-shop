import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';

const ModalDelete = ({onAgree, onDisagree, open = false, onClose, heading, fullWidth = true, title, maxWidth = 'sm'}) => {
  return (
    <Dialog
          fullWidth={fullWidth}
          maxWidth={maxWidth}
          open={open}
          onClose={onClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{title ? title : 'Delete confirmation!'}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {heading ? heading : 'Do you want to delete?'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={onDisagree} color="primary">
              No
            </Button>
            <Button onClick={onAgree} color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
  );
}

export default ModalDelete;