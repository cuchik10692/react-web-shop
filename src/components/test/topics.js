import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom'
import Topic from './topic';

function Topics ({ match }) {
  return (
    <div>
      <h1>Topics</h1>
      <ul><li><Link to={`${match.url}/sub`} >test</Link></li></ul>
      <hr />

      <Route path={`${match.path}/sub`} component={Topic}/>
    </div>
  )
}

export default Topics;