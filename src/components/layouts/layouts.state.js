import ActivityStatus from "@common/enum/activity";

export class LayoutsState {
  /**
   *
   */
  activityStatus;
  isNavMobile;

  constructor() {
    this.isNavMobile = false;
    this.activityStatus = ActivityStatus.NoActivity;
  }

}