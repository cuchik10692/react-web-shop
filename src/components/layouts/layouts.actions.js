import { RootAction } from "@store/root.actions";
import { LayoutsTypes } from './layouts.types';

export class LayoutsActions extends RootAction {

  toggleNavMobile(param, resolve, reject) {
    return { type: LayoutsTypes.IS_NAV_MOBILE.ROOT, payload: param, meta: { resolve, reject } }
  }
}

export const layoutsActions = new LayoutsActions