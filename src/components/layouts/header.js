import React, { useEffect, useState } from 'react';
import { persistStore } from 'redux-persist';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { AppBar, Avatar, Button, Grid, Hidden, Toolbar, Tooltip, Typography, withStyles } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import LaunchIcon from '@material-ui/icons/Launch';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { withRouter } from "react-router-dom";
// import Tab from '@material-ui/core/Tab';
// import Tabs from '@material-ui/core/Tabs';
import { authActions } from '@containers/auth/auth.actions';
import { layoutsActions } from './layouts.actions';

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing.unit,
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
});

const titles = {
  '/admin/categories': 'Categories Page',
  '/admin/products': 'Products Page',
  '/admin/products/new': 'Thêm mới sản phẩm',
  '/admin/new': 'Thêm mới sản phẩm',
  '/admin': 'Dashboard Page',
};

const Header = (props) => {
  const [title, setTitle] = useState('Dashboard Page');
  const { classes, toggleNavMobile, location, logout, currentUser } = props;
  useEffect(() => {
    const title = titles[location.pathname];
    setTitle(title);
    document.title = title;
  },[location.pathname])

  const handleLogout = async () => {
    logout();
    persistStore(props).purge();
  }

  return (
    <React.Fragment>
      <AppBar color="primary" position="sticky" elevation={0}>
        <Toolbar>
          <Grid container spacing={8} alignItems="center">
            <Hidden smUp>
              <Grid item>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={toggleNavMobile}
                  className={classes.menuButton}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Hidden>
            <Grid item xs />
            <Grid item>
              <Typography className={classes.link} component="a" href="#">
                Go to docs
              </Typography>
            </Grid>
            <Grid item>
              <Tooltip title="Alerts • No alters">
                <IconButton color="inherit">
                  <NotificationsIcon />
                </IconButton>
              </Tooltip>
            </Grid>
            <Grid item>
              <IconButton color="inherit" className={classes.iconButtonAvatar}>
                <Avatar className={classes.avatar} src="/static/images/avatar/1.jpg" />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <AppBar
        component="div"
        className={classes.secondaryBar}
        color="primary"
        position="static"
        elevation={0}
      >
        <Toolbar>
          <Grid container alignItems="center" spacing={8}>
            <Grid item xs>
              <Typography color="inherit" variant="h5">
                {title}
              </Typography>
            </Grid>
            <Grid item>
              <Button className={classes.button} variant="outlined" color="inherit" size="small">
              {currentUser.fullname ? currentUser.fullname : ''}
              </Button>
            </Grid>
            <Grid item>
              <Tooltip title="Logout">
                <IconButton color="inherit" onClick={handleLogout}>
                  <LaunchIcon />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      {/* <AppBar
        component="div"
        className={classes.secondaryBar}
        color="primary"
        position="static"
        elevation={0}
      >
        <Tabs value={0} textColor="inherit">
          <Tab textColor="inherit" label="Users" />
          <Tab textColor="inherit" label="Sign-in method" />
          <Tab textColor="inherit" label="Templates" />
          <Tab textColor="inherit" label="Usage" />
        </Tabs>
      </AppBar> */}
    </React.Fragment>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (rootReducer) => {
  return {
    currentUser: rootReducer.authReducer.currentUser,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
    logout: () => {
      dispatch(authActions.logout())
    },
    toggleNavMobile: () => {
      dispatch(layoutsActions.toggleNavMobile())
    }
  }
} 

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(Header)));