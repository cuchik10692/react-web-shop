import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import { withStyles, Divider, ListItem, ListItemIcon, ListItemText, Drawer, List } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import DnsRoundedIcon from '@material-ui/icons/DnsRounded';
import HomeIcon from '@material-ui/icons/Home';
import Hidden from '@material-ui/core/Hidden';
import classNames from 'classnames';
import { NavLink, withRouter } from "react-router-dom";
import { layoutsActions } from './layouts.actions';

const drawerWidth = 256;

const styles = theme => ({
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  categoryHeader: {
    paddingTop: 16,
    paddingBottom: 16,
  },
  categoryHeaderPrimary: {
    color: theme.palette.common.white,
  },
  item: {
    paddingTop: 4,
    paddingBottom: 4,
    color: 'rgba(255, 255, 255, 0.7)',
  },
  itemCategory: {
    backgroundColor: '#232f3e',
    boxShadow: '0 -1px 0 #404854 inset',
    paddingTop: 16,
    paddingBottom: 16,
  },
  firebase: {
    fontSize: 24,
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.common.white,
  },
  itemActionable: {
    '&:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.08)',
    },
  },
  itemActiveItem: {
    color: '#4fc3f7',
  },
  itemPrimary: {
    color: 'inherit',
    fontSize: theme.typography.fontSize,
    '&$textDense': {
      fontSize: theme.typography.fontSize,
    },
  },
  textDense: {},
  divider: {
    marginTop: theme.spacing.unit * 2,
  },
});

const categories = [
  {
    id: 'Products',
    children: [
      { id: 'Categories', icon: <PeopleIcon />, path: '/admin/categories' },
      { id: 'Products', icon: <DnsRoundedIcon />, path: '/admin/products' },
    ],
  }
];

const Nav = (classes, location, other) => {
  return (
    <Drawer variant="permanent" PaperProps={{ style: { width: drawerWidth } }} { ...other }>
      <List disablePadding>
        <ListItem className={classNames(classes.firebase, classes.item, classes.itemCategory)}>
          Admin
        </ListItem>
        <ListItem className={classNames(classes.item, classes.itemCategory)}
          component={NavLink} to="/admin" activeClassName={location.pathname === '/admin' ? classes.itemActiveItem : 'test'}
        >
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText
            classes={{
              primary: classes.itemPrimary,
            }}
          >
            Dashboard
          </ListItemText>
        </ListItem>
        {categories.map(({ id, children }) => (
          <React.Fragment key={id}>
            <ListItem className={classes.categoryHeader}>
              <ListItemText
                classes={{
                  primary: classes.categoryHeaderPrimary,
                }}
              >
                {id}
              </ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, path }) => (
              <ListItem
                button
                dense
                key={childId}
                className={classNames(
                  classes.item,
                  classes.itemActionable,
                )}
                component={NavLink}
                to={path}
                activeClassName={classes.itemActiveItem}
                // onlyActiveOnIndex
              >
                  <ListItemIcon>{icon}</ListItemIcon>
                  <ListItemText
                    classes={{
                      primary: classes.itemPrimary,
                      textDense: classes.textDense,
                    }}
                  >
                    {childId}
                  </ListItemText>
              </ListItem>
            ))}
            <Divider className={classes.divider} />
          </React.Fragment>
        ))}
      </List>
    </Drawer>
  );
}

const Navigator = ({classes, staticContext, isNavMobile, toggleNavMobile, location, ...other}) => {
  return (
    <nav className={classes.drawer}>
      <Hidden smUp implementation="js">
        {Nav(classes, location, {
          variant: 'temporary',
          open : isNavMobile,
          onClose: toggleNavMobile
        })}
      </Hidden>
      <Hidden xsDown implementation="css">
        {Nav(classes, location)}
      </Hidden>
    </nav>
  );
}


const mapStateToProps = (rootReducer) => {
  return {
    isNavMobile: rootReducer.layoutsReducer.isNavMobile,
  };
}

const mapDispatchToProps = {
  toggleNavMobile: layoutsActions.toggleNavMobile,
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(Navigator)));