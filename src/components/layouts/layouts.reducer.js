import { LayoutsState } from './layouts.state'
import { LayoutsTypes } from './layouts.types';

const initialLayoutsState = new LayoutsState();

export const layoutsReducer =
  (state = initialLayoutsState, action) => {
    switch (action.type) {
      case LayoutsTypes.IS_NAV_MOBILE.ROOT:
        return { ...state, isNavMobile: !state.isNavMobile }
      default:
        return state;
    }
  }