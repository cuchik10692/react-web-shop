import { asyncActionType } from '@utils/methods';

export const LayoutsTypes = {
  IS_NAV_MOBILE: asyncActionType('IS_NAV_MOBILE'),
}