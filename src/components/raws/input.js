import React from 'react';

import { Input } from '@material-ui/core';

const _Input = (props) => {
  return (
    <Input {...props} />
  );
}

export default _Input;