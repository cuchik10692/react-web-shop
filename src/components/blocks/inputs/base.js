import React from 'react';

import Input from '@components/raws/input';
import { FormControl, InputLabel, FormHelperText } from '@material-ui/core';

const BaseInputBlock = ({
  input,
  label,
  meta: { touched, error, warning  },
  ...custom
}) => {
  return (
    <FormControl error={touched && error ? true : false}>
      <InputLabel htmlFor={input.name}>{label}</InputLabel>
      <Input
        {...input}
        {...custom}
      />
      {touched && ((error && <FormHelperText >{error}</FormHelperText>) || (warning && <span>{warning}</span>))}
    </FormControl>
)}

export default BaseInputBlock;