import { regexPatter } from '@common/constants/pattern';

export const required = (fields, values) => {
  const errors = {}
  fields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

export const checkLength = (fieldsObj, values) => {
  const errors = {};
  fieldsObj.forEach(fieldObj => {
    const field = fieldObj.field;
    const limit = fieldObj.limit;
    const type = fieldObj.type;
    const compare = fieldObj.compare;
    if (type === 'value') {
      if (values[field] && values[field] > limit && compare === 'max') {
        errors[field] = (fieldObj.errorMsg) ? fieldObj.errorMsg : `Must be less than or equal ${limit}`;
      }
      if (values[field] && values[field] < limit && compare === 'min') {
        errors[field] = (fieldObj.errorMsg) ? fieldObj.errorMsg : `Must be greater than or equal ${limit}`;
      }
    } else {
      if (values[field] && values[field].length > limit && compare === 'max') {
        errors[field] = (fieldObj.errorMsg) ? fieldObj.errorMsg : `Must be ${limit} characters or less`;
      }
      if (values[field] && values[field].length < limit && compare === 'min') {
        errors[field] = (fieldObj.errorMsg) ? fieldObj.errorMsg : `Must be ${limit} characters or greater`;
      }
    }
    
  });
  return errors;
}


export const checkConfirmation = (fieldObj, values) => {
  const errors = {};
  const field = fieldObj.field;
  const confirmationField = fieldObj.confirmationField;
  const errorMsg = (fieldObj.errorMsg) ? fieldObj.errorMsg : 'Confirmation value is not match';
  if (values[field] !== values[confirmationField]) {
    errors[confirmationField] = errorMsg;
  }
  return errors;
}

export const checkEmail = (fieldObj, values) => {
  const errors = {};
  const field = fieldObj.field;
  const errorMsg = (fieldObj.errorMsg) ? fieldObj.errorMsg : 'Email invalid';
  if (!regexPatter.email.test(String(values[field]).toLowerCase())) {
    errors[field] = errorMsg;
  }
  return errors;
}