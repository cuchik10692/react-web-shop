export const formatCurrency = (value) => {
  return new Intl.NumberFormat('vi', { 
    style: 'currency', 
    currency: 'VND',
    currencyDisplay: 'code',
    minimumFractionDigits: 0, 
    maximumFractionDigits: 0 
  }).format(value)
}

export const asyncActionType = (type) => ({
  ROOT: `${type}`,
  LOADING: `${type}_LOADING`,
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  RESET: `${type}_RESET`,
});