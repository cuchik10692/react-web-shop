export const AuthUrl = {
  Login: 'login/',
  Register: 'register/',
  CurrentUser: 'account/',
}