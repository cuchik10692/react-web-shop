import { combineReducers, Reducer } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import { productsReducer, productForm } from '@containers/admin/products/products.reducer';
import { authReducer, authForm } from '@containers/auth/auth.reducer';
import { fileUploadReducer } from '@components/shared/file-upload/file-upload.reducer';
import { layoutsReducer } from '@components/layouts/layouts.reducer';

const persistConfig = {
  key: 'root',
  storage: storage,
  stateReconciler: autoMergeLevel2, // Xem thêm tại mục "Quá trình merge".
  log: true,
  whitelist: ['authReducer'],
 };

const reducers = combineReducers({
  productsReducer,
  authReducer,
  form: reduxFormReducer.plugin({ ...productForm, ...authForm }),
  fileUploadReducer,
  layoutsReducer,
})


const pReducer = persistReducer(persistConfig, reducers);

export default pReducer
