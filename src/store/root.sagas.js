import { all, fork } from "redux-saga/effects";

import { watchFetchProducts, watchDeleteProducts, watchCreateProduct, watchUpdateProduct } from '@containers/admin/products/products.sagas';
import { watchUploadImage, watchDeleteImage } from '@components/shared/file-upload/file-upload.sagas';
import { watchLogin, watchCurrentUser, watchRegister } from '@containers/auth/auth.sagas';
export function* rootSaga() {
  yield all([
    fork(watchFetchProducts),
    fork(watchDeleteProducts),
    fork(watchCreateProduct),
    fork(watchUpdateProduct),
    fork(watchUploadImage),
    fork(watchDeleteImage),
    fork(watchLogin),
    fork(watchRegister),
    fork(watchCurrentUser),
  ])
}