import React from 'react';
import ReactDOM from 'react-dom';
import { SnackbarProvider } from 'notistack';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import '@utils/configurations';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/lib/integration/react';
import store from '@store/index';
import FullScreenLoading from '@components/shared/loading/full-screen';

const persistor = persistStore(store);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={<FullScreenLoading />} persistor={persistor}>
      <SnackbarProvider 
        maxSnack={5}  
        anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
        }}
        autoHideDuration={2000}
      >
        <App />
      </SnackbarProvider>
    </PersistGate>
  </Provider>
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
